import React, {Component} from 'react';
import CardList from './CardList';
import Scroll from './Scroll';
import SearchBox from './SearchBox';
import './App.css';
class App extends Component{
    constructor(){
        super()
        this.state = {
            robots: [],
            searchField: '',
            err: 0
        }
    }

componentDidMount(){
    // https://jsonplaceholder.typicode.com/users
    fetch('https://facedetectionapis.herokuapp.com/users')
    .then(response => response.json())
    .then(users => this.setState({robots: users}))
    .catch(err => {
        return this.setState({err: 1});
    });

}

onSearchChange = (event) =>{
    this.setState({searchField: event.target.value});
}

    render(){ 
            const filteredRobots = this.state.robots.filter(robot => {
                return robot.name.toLowerCase().includes(this.state.searchField.toLowerCase());
            });
        
        
        
        if(this.state.err === 1){
            return <h1 className="tc">Unable To Fetch Friends..Try Again </h1>;
        }
       else if(this.state.robots.length === 0){
            return <h1 className="tc">Loading</h1>
       }else{
            return (
                <div className="tc">
                <h1 className="f2">RoboFriends</h1>
                <SearchBox searchChange = {this.onSearchChange}/>
                <Scroll>
                <CardList robots= {filteredRobots} />
                </Scroll>
                </div>
            ); 
       }
        
    }
}


export default App;